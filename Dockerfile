FROM python:alpine 

MAINTAINER Dirk Gently

#RUN pip install flask
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt


COPY app /app/

EXPOSE 5000

ENTRYPOINT ["python", "/app/app.py"]
